import React, { Fragment, useEffect } from "react"
import ReactDOM from "react-dom"
import store from 'redux-store'
import routes from "routes"
import history from 'utils/history'
import { BrowserRouter, Router,  Route, Switch, Redirect } from "react-router-dom"
import { Provider } from 'react-redux'

import { renderRoutes } from 'utils/routes'

import "react-notification-alert/dist/animate.css";
import "assets/css/argon-dashboard-pro.min.css"
import "assets/vendor/@fortawesome/fontawesome-free/css/all.min.css"
import "assets/scss/argon-dashboard-react.scss"
import "assets/vendor/select2/select2.css";
import "assets/vendor/nucleo/css/nucleo.css"

ReactDOM.render(
	<Provider store={store}>
		<Router history={history}>
			{renderRoutes(routes)}
		</Router>
	</Provider>,
  document.getElementById("root")
)
