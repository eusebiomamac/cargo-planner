import { combineReducers } from 'redux-starter-kit'
import shipment, { ShipmentActions } from 'redux-store/reducers/shipment'
import notification, { NotificationActions } from 'redux-store/reducers/notification'

export { ShipmentActions, NotificationActions }
export default combineReducers({
  shipment,
  notification,
})