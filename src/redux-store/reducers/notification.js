import { createSlice } from 'redux-starter-kit'

const initialState = {
  title: null,
  place: 'tr',
  type: 'info',
  icon: 'ni ni-bell-55',
  autoDismiss: 4,
  message: null,
}

const reducers = {
  notify(state, { payload }) {
    return { ...initialState, ...payload }
  }
}

const NotificationSlice = createSlice({ 
  name: 'notification', 
  initialState, 
  reducers
})

export const NotificationActions = NotificationSlice.actions
export default NotificationSlice.reducer