import { createSlice, createAction } from 'redux-starter-kit'

const initialState = {
  data: null,
  shipment: null,
  keyword: null,
  loading: false,
  updated: false,
}

const reducers = {
	setData(state, { payload }) {
		return { ...state, ...payload }
	},
	setShipments(state, { payload }) {
		state.data = payload
	},
	setShipment(state, { payload }) {
		state.shipment = payload
	},
  setLoading(state, { payload }) {
  	state.loading = payload
  },
  clearShipment(state, action) {
  	return initialState
  },
  findShipment(state, { payload }) {
  	state.keyword = payload
  }
}

const ShipmentSlice = createSlice({
  name: 'shipment',
  initialState,
  reducers
})

const CUSTOM_ACTIONS = {
  requestShipmentLoad: createAction('shipment/request-shipment-load'),
  requestShipmentData: createAction('shipment/request-shipment-data'),
  requestShipmentUpdate: createAction('shipment/request-shipment-update'),
  requestShipmentStorage: createAction('shipment/request-shipment-storage'),
}

export const ShipmentActions = { ...ShipmentSlice.actions, ...CUSTOM_ACTIONS }
export default ShipmentSlice.reducer