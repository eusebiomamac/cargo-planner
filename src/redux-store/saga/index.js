import { all, fork } from 'redux-saga/effects'
import shipment from 'redux-store/saga/shipment'

export default function* rootSaga() {
  yield all([
  	fork(shipment)
  ])
}