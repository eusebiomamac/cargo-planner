import axios from 'utils/axios'
import history from 'utils/history'
import storage from 'store2'
import findIndex from 'lodash/findIndex'
import { all, put, takeEvery, call, select } from 'redux-saga/effects'
import {
	ShipmentActions,
	NotificationActions
} from 'redux-store/reducers'

const { notify } = NotificationActions
const {
	setData,
	setLoading,
	setShipment,
	setShipments,
	requestShipmentData,
	requestShipmentLoad,
	requestShipmentUpdate,
	requestShipmentStorage,
} = ShipmentActions

const SHIPMENTS = 'shipments'
function request(params) {
  return axios
	.request(params)
	.then(response => response.data)
}

function forwardTo(location) {
  history.push(location)
}

function getErrorMessage(object) {
	try {
		return object.response.data.message
	} catch(error) {
		return object.message || object
	}
}

function* getShipment({ payload: shipment_id }) {
	try {
		yield put(setLoading(true))
		const shipments = yield select(state => state.shipment.data)

		if ((shipments && !shipments.length) || !shipments)
			throw 'No local saved shipments, Please load set shipments.'

		const shipment = shipments.find(({ id }) => id === shipment_id)

		if (!shipment) {
			return yield all([
				put(setLoading(false)),
				put(notify({
					title: 'Danger',
					message: 'Invalid Shipment id',
					type: 'danger',
					icon: 'fa fa-exclamation-triangle'
				}))
			])
		}

		yield all([
			put(setLoading(false)),
			put(setShipment(shipment))
		])

	} catch(error) {
		yield all([
			put(setLoading(false)),
			put(notify({
				title: 'Danger',
				message: getErrorMessage(error),
				type: 'warning',
				icon: 'fa fa-exclamation-triangle'
			}))
		])
	}
}

function* loadFromStorage() {
	try {
		yield put(setLoading(true))

		if (!storage.has(SHIPMENTS))
			throw 'No local saved shipments, Please load set shipments.'

		yield all([
			put(setLoading(true)),
			put (setShipments(storage.get(SHIPMENTS)))
		])
	} catch(error) {
		yield all([
			put(setLoading(false)),
			put(notify({
				title: 'Warning',
				message: getErrorMessage(error),
				type: 'warning',
				icon: 'fa fa-exclamation-triangle'
			}))
		])
	}
}

function* loadShipments() {
	try {
		yield put(setLoading(true))
		const data = yield call(request, {
			url: '/shipments',
			method: 'GET'
		})

		storage.set(SHIPMENTS, data)
		yield all([
			put(setLoading(false)),
			put(setShipments(data)),
			put(notify({
				title: 'Success',
				message: 'Shipments Loaded Successfully',
				type: 'success',
				icon: 'ni ni-check-bold'
			}))
		])
	} catch(error) {
		yield all([
			put(setLoading(false)),
			put(notify({
				title: 'Warning',
				message: getErrorMessage(error),
				type: 'warning',
				icon: 'fa fa-exclamation-triangle'
			}))
		])
	}
}

function* updateStorage({ payload }) {
	try {
		yield put(setLoading(true))

		if (!storage.has(SHIPMENTS))
			throw 'No local saved shipments, Please load set shipments.'

		const shipments = storage.get(SHIPMENTS)
		const index = findIndex(shipments, {id: payload.id})
		shipments.splice(index, 1, payload)
		storage.set(SHIPMENTS, shipments)

		yield all([
			put(setLoading(false)),
			put(requestShipmentStorage()),
			put(notify({
				title: 'Success',
				message: 'Shipment Updated Successfully',
				type: 'success',
				icon: 'ni ni-check-bold'
			}))
		])
	} catch(error) {
		yield all([
			put(setLoading(false)),
			put(notify({
				title: 'Warning',
				message: getErrorMessage(error),
				type: 'warning',
				icon: 'fa fa-exclamation-triangle'
			}))
		])
	}
}

export default function* watch() {
  yield takeEvery(requestShipmentData().type, getShipment)
  yield takeEvery(requestShipmentLoad().type, loadShipments)
  yield takeEvery(requestShipmentUpdate().type, updateStorage)
  yield takeEvery(requestShipmentStorage().type, loadFromStorage)
}