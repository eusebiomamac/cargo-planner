import { configureStore, getDefaultMiddleware } from "redux-starter-kit"
import createSagaMiddleware from 'redux-saga'
import rootReducer from 'redux-store/reducers'
import rootSaga from 'redux-store/saga'

const sagaMiddleware = createSagaMiddleware()
const store = configureStore({ 
	devTools: true, 
	reducer: rootReducer, 
	middleware: [ 
		sagaMiddleware,
		...getDefaultMiddleware({
	    serializableCheck: false,
	  }), 
	]
})

sagaMiddleware.run(rootSaga)

export default store