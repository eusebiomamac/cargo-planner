import React, { Fragment, useEffect, useMemo, useState } from "react"
import { useRouter, useDispatch, useSelector, createSelector } from 'hooks'
import Header from "components/Header"
import Search from 'components/Search'
import sum from 'lodash/sum'
import { ShipmentActions } from 'redux-store/reducers'
import {
  Badge,
  Button,
  Input,
  Container,
  CardHeader,
  CardBody,
  Card,
  FormGroup,
} from "reactstrap"

const {
  clearShipment,
  findShipment,
  requestShipmentData,
  requestShipmentLoad,
  requestShipmentUpdate,
  requestShipmentStorage,
} = ShipmentActions

const defaultFields = {
	boxes: '',
	email: '',
	id: '',
	name: '',
}

const errors ={}

function Shipment({ theme = 'success', ...props}) {
  const { query: { id: shipment_id = '' } } = useRouter()
  const dispatch = useDispatch()
  const [ fields, setFields ] = useState(defaultFields)
  const { shipments, shipment, loading } = useSelector(createSelector(
    state => state.shipment.data,
    state => state.shipment.shipment,
    state => state.shipment.loading,
    (data, shipment, loading) => ({
    	shipments: data, shipment, loading
    })
  ))

  useEffect(() => {
  	dispatch(requestShipmentStorage())
  }, [])

  useEffect(() => {
  	shipment_id && shipments && dispatch(requestShipmentData(shipment_id))
  }, [shipment_id, shipments])

  useEffect(() => {
  	if (shipment) {
  		const boxes = shipment.boxes !== null
  			? shipment.boxes
  			: ''
  		setFields({ ...shipment, boxes })
  	}
  }, [shipment])

  return (
    <Fragment>
      <Header theme={theme}/>
      <Container className="mt--9" fluid>
      	<div className="row">
      		<div className="col">
      			<Card className="card-frame">
		          <CardHeader>
		            <div className="align-center text-left flex-left overflow-auto">
		            	<Search onChange={handleSearch} />
		            	<div className="col-md-4">
		            		<Button onClick={handleLoadShipments} className="col-md-5" color="secondary">Load</Button>
		            		{shipment && (
		            			<Button onClick={handleSave} className="col-md-5" color="primary">Save</Button>
		            		)}
		            	</div>
		            </div>
		          </CardHeader>
		        </Card>
      		</div>
      	</div>
      	<div className="row">
      		<div className="col">
      			<Card className="card-frame">
		          <CardBody>
		            <div className="text-center">
		            	{!shipments && (
		            		<p>No local saved shipments, Please load set shipments.</p>
		            	)}
		            	{shipments && !shipment_id && (
		            		<p>Please select a shipment to view</p>
		            	)}
		            	{shipments && shipment_id && !loading && !shipment && (
		            		<p>The shipment you are looking for, cannot be found</p>
		            	)}
		            </div>
		            {shipment && (
		            	<Fragment>
		            		<h1>{fields.name}</h1>
				            <p><b>{fields.email}</b></p>
				            <p>Number of required cargo bays: <b>{calculateCargoBays(fields.boxes)}</b></p>
				            <FormGroup className="row col-md-5">
				            	<p>Cargo Boxes: </p>
					            <Input
					              onChange={handleChange('boxes')}
					              value={fields.boxes}
					              placeholder='Boxes'
					              id="name"
					              type="text"
					            />
					            {errors.name && (
					          		<div className='custom-invalid-select'>
					          			{errors.name}
					          		</div>
					            )}
					          </FormGroup>
		            	</Fragment>
		            )}
		          </CardBody>
		        </Card>
      		</div>
      	</div>
      </Container>
    </Fragment>
  )

  function handleSave() {
  	dispatch(requestShipmentUpdate(fields))
  }

  function handleLoadShipments() {
  	dispatch(requestShipmentLoad())
  }

  function handleChange(element) {
  	return event => {
  		const { value } = event.target
  		setFields(fieds => ({ ...fieds, [element]: value }))
  	}
  }

  function handleSearch(keyword) {
  	dispatch(findShipment(keyword))
  }

  function calculateCargoBays(boxes) {
  	try {
  		boxes = boxes
  			.split(',')
  			.map(box => isNaN(Number(box)) ? 0 : Number(box))

  		return Math.ceil(sum(boxes) / 10)
  	} catch(err) {
  		return 0
  	}
  }
}

export default Shipment;
