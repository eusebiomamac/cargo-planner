import React, { useCallback, useEffect, useRef } from "react"
import { debounce } from 'lodash'
import cn from 'classnames'

export default function Search({ className, onChange, value: keyword }) {
  const textInput = useRef()

  useEffect(() => {
    if (keyword && textInput.current)
      textInput.current.value = keyword
  }, [])

  const handleChange = useCallback(debounce(handleSearch, 1000))

  return (
    <div className={cn('input-group', className)}>
      <div className="input-group-prepend">
          <span className="input-group-text">
            <i className="fa fa-search"></i>
          </span>
      </div>
      <input
        type="text"
        className="form-control"
        placeholder="Search"
        onChange={event => handleChange(event.target.value)}
        ref={textInput}
      />
    </div>
  )

  function handleSearch(value) {
    if (onChange)
      onChange(value)
  }
}
