import React, { Fragment } from "react"
import { NavLink as NavLinkRRD, Link } from "react-router-dom"
import { createSelector, useSelector } from 'hooks'
import {
	Container,
	NavbarBrand,
	Collapse,
	NavItem,
	NavLink,
	Navbar,
	Nav,
} from "reactstrap"

export default function Sidebar({ routes, logo, ...props}) {
	const { shipments, keyword } = useSelector(createSelector(
    state => state.shipment.data,
    state => state.shipment.keyword,
    (data, keyword) => ({ shipments: data, keyword })
  ))

	return (
    <Navbar
      className="navbar-vertical fixed-left navbar-light bg-white"
      expand="md"
      id="sidenav-main"
    >
      <Container fluid>
      	{logo && (
      		 <NavbarBrand className="pt-0">
            <img
              alt={logo.imgAlt}
              className="navbar-brand-img"
              src={logo.imgSrc}
            />
          </NavbarBrand>
      	)}
        <Collapse navbar>
          <Nav navbar>{createLinks()}</Nav>
        </Collapse>
      </Container>
    </Navbar>
  )

  function createLinks() {
  	const icon = 'fa fa-address-card'
  	const base = '/shipment'

  	let data = (shipments || [])

  	if (keyword)
  		data = data.filter(({ name }) => {
  			return name.toLowerCase().includes(keyword)
  		})

	  return data.map(({ name, id }, index) => (
  		<NavItem key={index}>
        <NavLink
          to={`${base}/${id}`}
          tag={NavLinkRRD}
          activeClassName="active text-success"
        >
          <i className={icon} />
          <span>{name}</span>
        </NavLink>
      </NavItem>
  	))
  }
}
