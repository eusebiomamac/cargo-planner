import React, { Fragment } from "react";

function Header({ theme = 'info' }) {
  return (
    <Fragment>
      <div className={`header bg-gradient-${theme} pb-8 pt-5 pt-md-8`} />
    </Fragment>
  )
}

export default Header
