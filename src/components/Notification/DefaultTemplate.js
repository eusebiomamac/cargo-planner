import React from "react";

export default function DefaultTemplate({ title, message }) {
  return (
    <div className="alert-text">
      <span className="alert-title" data-notify="title">
        {" "}{title}
      </span>
      <span data-notify="message">{message}</span>
    </div>
  )
}
