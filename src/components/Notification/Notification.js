import React, { Fragment, useRef, useEffect } from "react";
import { useSelector } from 'react-redux'
import { createSelector } from 'reselect'
import NotificationAlert from 'react-notification-alert'
import DefaultTemplate from 'components/Notification/DefaultTemplate'

export default function Notification() {
  const notify = useRef(null)
  const data = useSelector(createSelector(
    state => state.notification,
    (notification) => notification
  ))

  useEffect(() => {
    const { message, title } = data
    if (message)
      notify.current.notificationAlert({ 
        ...data,
        message: (
          <DefaultTemplate 
            message={message} 
            title={title}
          />
        )
      })
  }, [data])

  return (
    <div className="toast-notification">
      <NotificationAlert ref={notify} />
    </div>
  )
}
