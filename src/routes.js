import React, { lazy } from 'react'

const MainLayout 	= lazy(() => import('layouts/MainLayout'))
const Shipment 		= lazy(() => import('container/Shipment'))

const NotFound = () => (
	<div className="not-found">
		<p className="title"><b>404: </b>Page not Found</p>
		<p className="desc">The page you are looking for cannot be found</p>
	</div>
)

const routes = [
  {
    path: "/shipment/:id?",
    name: "Shipment",
    key: "route-shipment",
    Component: Shipment,
    Layout: MainLayout,
    exact: true
  },
  {
    path: "/notfound",
    name: "Not Found",
    key: "route-notfound",
    Component: NotFound,
    exact: true
  }
]

export default routes