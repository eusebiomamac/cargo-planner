import React, { Suspense, Fragment } from 'react'
import { Route, Switch, Redirect  } from "react-router-dom"
import Notification from 'components/Notification/Notification'

export function findRoute(routes, name) {
  const keyword = String(name).toLowerCase()
  return routes.find(route => {
    return route.name
      .toLowerCase()
      .includes(keyword)
  })
}

export function render({ props, route, Component, Layout, routes }) {
  if (Layout)
    return (
      <Layout route={route} { ...props }>
        <Component route={route} { ...props }/>
      </Layout>
    )

  return <Component route={route} { ...props }/>
}

export function renderRoutes(routes) {
  if (!routes)
    return null

  return (
    <Fragment>
    	<Suspense fallback={<div>Loading, please wait...</div>}>
	      <Notification/>
	      <Switch>
	        {routes.map(({ Component, Layout, ...route }, index) => (
	          <Route { ...route } render={props =>
	              render({ props, route, Component, Layout, routes })
	            }
	          />
	        ))}
	        <Redirect exact from="/" to="/shipment" />
	        <Redirect to="/notfound" />
	      </Switch>
	    </Suspense>
    </Fragment>
  )
}