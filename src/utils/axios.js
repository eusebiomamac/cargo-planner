import axios, { CancelToken as CT } from 'axios'

const {REACT_APP_API} = process.env
const errorCallback = error => {
	return Promise.reject(error)
}

const requestCallback = request => {
	if (!request.url.includes('http'))
		request.url = `${REACT_APP_API}${request.url}`

	return request
}

const responseCallback = response => {
	return response
}

axios.interceptors.request.use(requestCallback, errorCallback)
axios.interceptors.response.use(responseCallback, errorCallback)

export const CancelToken = CT
export default axios
