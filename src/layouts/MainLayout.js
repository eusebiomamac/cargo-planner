import React, { Fragment } from "react"
import { Container } from "reactstrap"
import Sidebar from "components/Sidebar"
import routes from "routes"

export default function MainLayout({ children, ...props }) {
  const logo = {
    innerLink: "/dashboard",
    imgSrc: require("assets/img/brand/logo.png"),
    imgAlt: "..."
  }
  const sidebar = { ...props, routes, logo }
  return (
    <Fragment>
      <Sidebar { ...sidebar } />
      <div className="main-content">
        { children }
      </div>
    </Fragment>
  )
}