
# Cargo Planner

SpaceX is experiencing an increasing demand of shipments to Mars and has commissioned an application to automate the needed cargo space calculations.

## Available Scripts

### `start`
In the project directory, you can run:

    npm start

This command will run both
```npm run start:web``` and ```npm run start:server```

### `start:web`
In the project directory, you can run:

    npm run start:web

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.

### `start:server`
In the project directory, you can run:

    npm run start:server

Launches the json-server.
To view the json-server go to [http://localhost:3005/shipments](http://localhost:3005/shipments)

